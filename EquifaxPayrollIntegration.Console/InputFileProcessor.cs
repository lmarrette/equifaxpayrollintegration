﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using DataLayer;
using DataLayer.Models;
using EquifaxPayrollIntegration.Console.IO;
using EquifaxPayrollIntegration.Console.Properties;
using FileHelpers;
using log4net;

namespace EquifaxPayrollIntegration.Console
{
    /// <summary>
    ///     Load files from SFTP site to staging table
    /// </summary>
    public class InputFileProcessor
    {
        private const string fileNameFormat = "EquifaxPayroll*.csv";

        private static readonly ILog log = LogManager.GetLogger
            (MethodBase.GetCurrentMethod().DeclaringType);

        private readonly ContextEquifaxPayroll _context;
        private readonly PayrollStagingDb      _db;

        private string[]      _filesToProcess;
        private string        _localDirectory;
        private DirectoryInfo dir;
        private FileInfo[]    files;

        public InputFileProcessor()
        {
            _db      = new PayrollStagingDb();
            _context = new ContextEquifaxPayroll();
        }

        public void ProcessFiles()
        {
            _db.ClearStagingTable();
            _localDirectory = Settings.Default.PayrollFilePath;

            try
            {
                dir   = new DirectoryInfo(_localDirectory);
                files = dir.GetFiles(fileNameFormat);
                if (files.Length == 0)
                {
                    log.Info("Nothing to Process.");
                    Environment.Exit(0);
                }

                foreach (var file in files)
                {
                    var fName = _localDirectory + file.Name;


                    log.Info("Beginning file process for: " + fName);
                    var fileRowCount = File.ReadLines(fName).Count() - 1;
                    var processCount = 0;
                    var engine       = new FileHelperEngine<EquifaxInboundFile>();
                    var records      = engine.ReadFile(fName);

                    foreach (var record in records)
                    {
                        var payData = LoadPayrollInput(record);
                        //_db.InsertPayrollStagingData(payData);
                        _context.PayrollStagings.Add(payData);
                        processCount++;
                    }

                    _context.SaveChanges();
                    log.Info("Completed file processing for: " + file.Name +
                             ". Incoming file has " + fileRowCount +
                             " rows.  Processed: " +
                             processCount + " rows.");
                }
            }
            catch (Exception e)
            {
                log.Error("Exception processing file info: " + e);
                throw;
            }
        }

        private PayrollStaging LoadPayrollInput(EquifaxInboundFile record)
        {
            try
            {
                var ps = new PayrollStaging();
                ps.EmployeeId            = record.EmployeeId;
                ps.PeriodStartDate       = record.PeriodStartDate;
                ps.PeriodEndDate         = record.PeriodEndDate;
                ps.PayDate               = record.PayDate;
                ps.SocialSecurityNumber  = record.SocialSecurityNumber;
                ps.UserName              = record.UserName;
                ps.PayGroup              = record.PayGroup;
                ps.FirstName             = record.FirstName;
                ps.MiddleName            = record.MiddleName;
                ps.LastName              = record.LastName;
                ps.HireDate              = record.HireDate;
                ps.ContinuousServiceDate = record.ContinuousServiceDate;
                ps.JobTitle              = record.JobTitle;
                ps.ActiveStatus          = record.ActiveStatus;
                ps.OriginalHireDate      = record.OriginalHireDate;
                ps.TimeType              = record.TimeType;
                ps.TerminationDate       = record.TerminationDate;
                ps.HomeAddressLine1      = record.HomeAddressLine1;
                ps.HomeAddressLine2      = record.HomeAddressLine2;
                ps.HomeAddressCity       = record.HomeAddressCity;
                ps.HomeAddressState      = record.HomeAddressState;
                ps.HomeAddressPostalCode = record.HomeAddressPostalCode;
                ps.DateOfBirth           = record.DateOfBirth;
                ps.PeriodEndDateLastRegularPayPeriod =
                    record.PeriodEndDateLastRegularPayPeriod;
                ps.WorkAddressLine1   = record.WorkAddressLine1;
                ps.WorkAddressLine2   = record.WorkAddressLine2;
                ps.WorkCity           = record.WorkCity;
                ps.WorkState          = record.WorkState;
                ps.WorkPostalCode     = record.WorkPostalCode;
                ps.HourlyRate         = record.HourlyRate;
                ps.TotalBasePayAmount = record.TotalBasePayAmount;
                ps.CompanyTaxId       = record.CompanyTaxId;
                ps.DeductionCode      = record.DeductionCode;
                ps.EarningCode        = record.EarningCode;
                ps.ResultLineAmount   = record.ResultLineAmount;
                ps.BalanceYtd         = record.BalanceYtd;
                ps.HoursProrated      = record.HoursProrated;
                ps.HoursUnprorated    = record.HoursUnprorated;
                ps.EmployerPaid       = record.EmployerPaid.Replace(".", string.Empty);
                return ps;
            }
            catch (Exception e)
            {
                log.Error("Error in LoadPayrollInput for employee: " + record.EmployeeId +
                          " = " + e);
                throw;
            }
        }
    }
}