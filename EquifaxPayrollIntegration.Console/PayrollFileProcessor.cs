﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using DataLayer;
using log4net;

namespace EquifaxPayrollIntegration.Console
{
    /// <summary>
    /// Main processing class
    /// </summary>
    public partial class PayrollFileProcessor
    {
        private static readonly ILog log = LogManager.GetLogger
            (MethodBase.GetCurrentMethod().DeclaringType);

        private readonly SftpProcessor            _sftp;
        private readonly InputFileProcessor       _inputFileProcessor;
        private readonly DataProcessor            _dataProcessor;
        private readonly OutputFileProcessor      _outputFileProcessor;
        private readonly EmployeeBenefitProcessor _benefitProcessor;
        private readonly ContextEquifaxPayroll _db;

        public PayrollFileProcessor(SftpProcessor sftpProcessor)
        {
            _sftp                = new SftpProcessor();
            _inputFileProcessor  = new InputFileProcessor();
            _dataProcessor       = new DataProcessor();
            _outputFileProcessor = new OutputFileProcessor();
            _benefitProcessor    = new EmployeeBenefitProcessor();
            _db = new ContextEquifaxPayroll();
        }

        public void ProcessPayrollFile()
        {
    //      _sftp.RetrieveFiles();
            // load worker benefit data
    //        _benefitProcessor.LoadWorkerBenefits();
            // load file to staging
            _inputFileProcessor.ProcessFiles();
            LoadDemographicAndPayrollTables();
            //  process payroll staging data
            //        var formattedData = _dataProcessor.ProcessStagingData();
            // create output file
            //        _outputFileProcessor.CreateOutputFile(formattedData);
        }

        /// <summary>
        /// Call stored procedures to load demographic and payroll tables.
        /// </summary>
        private void LoadDemographicAndPayrollTables()
        {
            try
            {
                _db.Database.ExecuteSqlCommand("procLoadEmployeeDemographic");
                _db.Database.ExecuteSqlCommand("procLoadCurrentPayrollData");
                _db.Database.ExecuteSqlCommand("procLoadYtdEarnings");
            }
            catch (Exception e)
            {
                log.Error("Exception loading Demographic and Payroll Tables: " + e);
                throw;
            }
        }
    }
}