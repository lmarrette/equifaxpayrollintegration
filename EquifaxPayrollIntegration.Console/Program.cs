﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using EquifaxPayrollIntegration.Console.Properties;
using log4net;
using log4net.Config;

[assembly: XmlConfigurator(Watch = true)]

namespace EquifaxPayrollIntegration.Console
{
    class Program
    {
        private static readonly ILog log = LogManager.GetLogger
            (MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            log.Info("Equifax process started");
            try
            {
                // Archive files and clean up folder
    //            PrepareFolder();  COMMENTED OUT FOR TESTING

                var fileLoader = new PayrollFileProcessor(new SftpProcessor());
                fileLoader.ProcessPayrollFile();
                // Send file to Equifax via SFTP
                //    SendFiles();  COMMENTED OUT FOR TESTING
                // Email payroll that file was sent.
                //NotifyPayrollComplete();  COMMENTED OUT FOR TESTING
            }
            catch (Exception e)
            {
                string   mailFrom      = Settings.Default.MailFrom;
                string   subject       = Settings.Default.Subject;
                string[] recipientList = Settings.Default.Recipient.Split(',');
                var msg = "Exception in EquifaxPayroll: " + e + " - Stack Trace = " +
                          e.StackTrace + " - Inner Exception - " + e.InnerException;
                log.Error(msg);
                var email = new EmailNotification(mailFrom, subject, msg);
                email.SendNotificationEmail(recipientList, null);
            }
        }

        /// <summary>
        /// Archive in and out file and clean up folder
        /// </summary>
        private static void PrepareFolder()
        {
            try
            {
                log.Info("Starting Clear Output Files");
                string   sourcePath     = Settings.Default.PayrollFilePath;
                string[] filesToProcess = Directory.GetFiles(sourcePath, "*.*");
                foreach (var file in filesToProcess)
                {
                    if (file.Contains("prodin.v5")) File.Copy(file, Settings.Default.OutboundArchive + Path.GetFileName(file));
                    else if (file.Contains("EquifaxPayroll")) File.Copy(file, Settings.Default.InboundArchive + Path.GetFileName(file));
                    File.Delete(file);
                }
                log.Info("Finished Clearing output files");
            }
            catch (Exception e)
            {
                log.Error("Error clearing folder: " + e);
                throw;
            }
        }

    }
}
