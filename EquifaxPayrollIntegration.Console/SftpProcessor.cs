﻿// Created by:  Lee Marrette
// Date:  10/12/2018
// Solution:  EquifaxPayrollIntegration
// File:  SftpProcessor.cs

using System;
using EquifaxPayrollIntegration.Console.Properties;
using WinSCP;

namespace EquifaxPayrollIntegration.Console
{
    public class SftpProcessor
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger
    (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        SessionOptions _sessionOption;
        string _remoteFiles;
        string _localFolder;
        private const string FileName = "EquifaxPayroll*";

        public SftpProcessor(string outboundFile)
        {
            _sessionOption = new SessionOptions
            {
                Protocol = Protocol.Sftp,
                HostName = "sshftp.talx.com",
                UserName = "18617twn",
                Password = "E4kScPo3",  //TODO: Update password
                SshHostKeyFingerprint = "ssh-dss 1024 33:31:20:b0:56:4e:de:e7:8b:c6:ea:fd:58:07:49:0c"
            };
            _remoteFiles = Settings.Default.RemoteFolder + outboundFile; //_fileName + ".csv";

            _localFolder = outboundFile;
        }

        public SftpProcessor()
        {
            _sessionOption = new SessionOptions
            {
                Protocol = Protocol.Sftp,
                HostName = "sftp.tsged.com",
                UserName = "Workday",
                Password = "g%6K@WbU",
                SshHostKeyFingerprint = "ssh-rsa 1024 ae:e0:c0:d1:9b:9f:6e:51:19:e8:45:6a:d7:4e:17:9a"
            };
            _localFolder = Settings.Default.PayrollFilePath;
            _remoteFiles = Settings.Default.RemoteFolder + FileName + ".csv";
        }

        public void SendFiles()
        {

            try
            {
                Log.Info("Starting File Send.");
                using (Session session = new Session())
                {
                    session.Open(_sessionOption);
                    // Upload files
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;

                    TransferOperationResult transferResult;
                    transferResult = session.PutFiles(_localFolder, _remoteFiles, false, transferOptions);

                    transferResult.Check();

                    foreach (TransferEventArgs transfer in transferResult.Transfers)
                    {
                        Log.Info("Upload of " + transfer.FileName + " succeeded");
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error("Error sending files: " + e);
                throw;
            }
        }
        /// <summary>
        /// Retrieves files from SCP SFTP site created by payroll
        /// </summary>
        /// <returns></returns>
        public void RetrieveFiles()
        {
            try
            {
                Log.Info("Starting file retrieval.");
                using (Session session = new Session())
                {
                    session.Open(_sessionOption);
                    // Download files
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;

                    TransferOperationResult transferResult;
                    transferResult = session.GetFiles(_remoteFiles, _localFolder, false, transferOptions);

                    // Throw on any error
                    transferResult.Check();

                    foreach (TransferEventArgs transfer in transferResult.Transfers)
                    {
                        Log.Info("Download of " + transfer.FileName + " succeeded");
                    }
                    session.RemoveFiles(_remoteFiles);
                }
            }
            catch (Exception e)
            {

                Log.Error("Error retrieving files: " + e);
                throw;
            }
        }

    }
}