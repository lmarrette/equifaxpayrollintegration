﻿// Created by:  Lee Marrette
// Date:  10/12/2018
// Solution:  EquifaxPayrollIntegration
// File:  EquifaxInboundFile.cs

using FileHelpers;

namespace EquifaxPayrollIntegration.Console.IO
{
    [IgnoreFirst]
    [DelimitedRecord("|")]
    public class EquifaxInboundFile
    {
        public string EmployeeId;

        public string SocialSecurityNumber;
        public string UserName;
        public string PayGroup;
        public string FirstName;
        public string MiddleName;
        public string LastName;
        public string HireDate;
        public string ContinuousServiceDate;
        public string JobTitle;
        public string ActiveStatus;
        public string OriginalHireDate;
        public string TimeType;
        public string TerminationDate;
        public string HomeAddressLine1;
        public string HomeAddressLine2;
        public string HomeAddressCity;
        public string HomeAddressState;
        public string HomeAddressPostalCode;
        public string DateOfBirth;
        public string PeriodEndDateLastRegularPayPeriod;
        public string WorkAddressLine1;
        public string WorkAddressLine2;
        public string WorkCity;
        public string WorkState;
        public string WorkPostalCode;
        public string HourlyRate;
        public string TotalBasePayAmount;
        public string CompanyTaxId;
        public string PeriodStartDate;
        public string PeriodEndDate;
        public string PayDate;
        public string PayGroupDetail;
        public string DeductionCode;
        public string EarningCode;
        public string ResultLineAmount;
        public string BalanceYtd;
        public string HoursProrated;
        public string HoursUnprorated;
        public string EmployerPaid;
    }
}