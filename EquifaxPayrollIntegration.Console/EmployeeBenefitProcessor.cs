﻿using System;
using System.Data.Common;
using System.Reflection;
using DataLayer;
using DataLayer.Models;
using EquifaxPayrollIntegration.Console.EmployeeBenefitsServiceReference;
using log4net;
using WdConnect;


namespace EquifaxPayrollIntegration.Console
{
    /// <summary>
    /// Load benefit db for each worker. This info is used to report employee health
    /// coverage premiums.
    /// </summary>
    public class EmployeeBenefitProcessor
    {
        private static readonly ILog Log = LogManager.GetLogger
            (MethodBase.GetCurrentMethod().DeclaringType);

        ReportPortClient      client;
        ContextEquifaxPayroll db;
        WorkdayConnection     wd;

        /// <summary>
        /// Constructor
        /// </summary>
        public EmployeeBenefitProcessor()
        {
            client = new ReportPortClient("Benefits");
            db     = new ContextEquifaxPayroll();
            wd     = new WorkdayConnection();
        }

        public void LoadWorkerBenefits()
        {
            Log.Info("Beginning Benefit Loads");
            try
            {
                if (client.ClientCredentials != null)
                {
                    client.ClientCredentials.UserName.UserName = wd.UserName;
                    client.ClientCredentials.UserName.Password = wd.UserPw;
                }

                client.InnerChannel.OperationTimeout = TimeSpan.FromMinutes(15);
                ClearWorkerData();
                var request  = new Execute_ReportType();
                var response = client.Execute_Report(request);
                foreach (var t in response)
                {
                    var benefitData = AddEmployeeBenefit(t);
                    db.EmployeeBenefits.Add(benefitData);
                }

                db.SaveChanges();
            }
            catch (Exception e)
            {
                Log.Error("Exception Loading Worker Benefits: " + e);
            }

            Log.Info("Benefit Loads are complete");
        }

        /// <summary>
        /// Build employee benefit object
        /// </summary>
        /// <param name="ben">Workday benefit data</param>
        /// <returns>Benefit data</returns>
        private EmployeeBenefit AddEmployeeBenefit(Report_EntryType ben)
        {
            var benefit = new EmployeeBenefit
            {
                EmployeeId  = ben.worker.employeeId,
                BenefitPlan = ben.benefitPlan.Descriptor,
                Coverage    = ben.coverage
            };
            return benefit;
        }

        /// <summary>
        /// Remove existing data
        /// </summary>
        private void ClearWorkerData()
        {
            try
            {
                db.Database.ExecuteSqlCommand("TRUNCATE TABLE EmployeeBenefits");
            }
            catch (DbException e)
            {
                Log.Info("Database exception truncating Employee Benefits table: " + e);
                throw;
            }
            catch (Exception e)
            {
                Log.Info("General exception truncating Employee Benefits table: " + e);
                throw;
            }
        }
    }
}