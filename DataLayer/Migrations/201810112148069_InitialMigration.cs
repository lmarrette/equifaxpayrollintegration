namespace DataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CurrentPayrollDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeId = c.String(),
                        PayGroupDetail = c.String(),
                        PeriodStartDate = c.DateTime(nullable: false),
                        PeriodEndDate = c.DateTime(nullable: false),
                        PayDate = c.DateTime(nullable: false),
                        DeductionCode = c.String(),
                        EarningCode = c.String(),
                        ResultLineAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ProratedHours = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UnProratedHours = c.Decimal(nullable: false, precision: 18, scale: 2),
                        EmployerPaid = c.String(),
                        ValidCode = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EmployeeDemographics",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeId = c.String(),
                        SocialSecurityNumber = c.String(),
                        UserName = c.String(),
                        PayGroup = c.String(),
                        FirstName = c.String(),
                        MiddleName = c.String(),
                        LastName = c.String(),
                        HireDate = c.String(),
                        ContinuousServiceDate = c.String(),
                        JobTitle = c.String(),
                        ActiveStatus = c.String(),
                        OriginalHireDate = c.String(),
                        TimeType = c.String(),
                        TerminationDate = c.String(),
                        HomeAddressLine1 = c.String(),
                        HomeAddressLine2 = c.String(),
                        HomeAddressCity = c.String(),
                        HomeAddressState = c.String(),
                        HomeAddressPostalCode = c.String(),
                        DateOfBirth = c.String(),
                        PeriodEndLastRegularPayPeriod = c.String(),
                        WorkAddressLine1 = c.String(),
                        WorkAddressLine2 = c.String(),
                        WorkCity = c.String(),
                        WorkState = c.String(),
                        WorkPostalCode = c.String(),
                        HourlyRate = c.String(),
                        TotalBasePay = c.String(),
                        CompanyTaxId = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.YearToDateEarnings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeId = c.String(),
                        PayDate = c.DateTime(nullable: false),
                        Earning = c.String(),
                        EarningMapping = c.String(),
                        YtdAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.YearToDateEarnings");
            DropTable("dbo.EmployeeDemographics");
            DropTable("dbo.CurrentPayrollDetails");
        }
    }
}
