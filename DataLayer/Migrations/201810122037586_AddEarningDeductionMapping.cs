namespace DataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEarningDeductionMapping : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DeductionMappings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Deduction = c.String(),
                        CurrentMapping = c.String(),
                        YtdMapping = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EarningMappings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Earning = c.String(),
                        CurrentMapping = c.String(),
                        YtdMapping = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.EarningMappings");
            DropTable("dbo.DeductionMappings");
        }
    }
}
