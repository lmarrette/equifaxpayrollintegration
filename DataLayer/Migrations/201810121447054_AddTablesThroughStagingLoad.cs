namespace DataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTablesThroughStagingLoad : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EmployeeBenefits",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeId = c.String(),
                        BenefitPlan = c.String(),
                        Coverage = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PayrollStagings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeId = c.String(),
                        PeriodStartDate = c.String(),
                        PeriodEndDate = c.String(),
                        PayDate = c.String(),
                        GrossPay = c.String(),
                        NetPay = c.String(),
                        SocialSecurityNumber = c.String(),
                        UserName = c.String(),
                        PayGroup = c.String(),
                        FirstName = c.String(),
                        MiddleName = c.String(),
                        LastName = c.String(),
                        HireDate = c.String(),
                        ContinuousServiceDate = c.String(),
                        JobTitle = c.String(),
                        ActiveStatus = c.String(),
                        OriginalHireDate = c.String(),
                        TimeType = c.String(),
                        TerminationDate = c.String(),
                        HomeAddressLine1 = c.String(),
                        HomeAddressLine2 = c.String(),
                        HomeAddressCity = c.String(),
                        HomeAddressState = c.String(),
                        HomeAddressPostalCode = c.String(),
                        DateOfBirth = c.String(),
                        PeriodEndDateLastRegularPayPeriod = c.String(),
                        WorkAddressLine1 = c.String(),
                        WorkAddressLine2 = c.String(),
                        WorkCity = c.String(),
                        WorkState = c.String(),
                        WorkPostalCode = c.String(),
                        HourlyRate = c.String(),
                        TotalBasePayAmount = c.String(),
                        CompanyTaxId = c.String(),
                        DeductionCode = c.String(),
                        EarningCode = c.String(),
                        ResultLineAmount = c.String(),
                        BalanceYtd = c.String(),
                        HoursProrated = c.String(),
                        HoursUnprorated = c.String(),
                        EmployerPaid = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PayrollStagings");
            DropTable("dbo.EmployeeBenefits");
        }
    }
}
