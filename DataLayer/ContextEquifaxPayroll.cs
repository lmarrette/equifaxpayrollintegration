﻿// Created by:  Lee Marrette
// Date:  10/11/2018
// Solution:  EquifaxPayrollIntegration
// File:  ContextEquifaxPayroll.cs

using System.Data.Entity;
using DataLayer.Models;

namespace DataLayer
{
    public class ContextEquifaxPayroll : DbContext
    {
        public DbSet<EmployeeDemographic> EmployeeDemographics { get; set; }
        public DbSet<CurrentPayrollDetail> CurrentPayrollDetails { get; set; }
        public DbSet<YearToDateEarning> YearToDateEarnings { get; set; }
        public DbSet<EmployeeBenefit> EmployeeBenefits { get; set; }
        public DbSet<PayrollStaging> PayrollStagings { get; set; }
        public DbSet<EarningMapping> EarningMappings { get; set; }
        public DbSet<DeductionMapping> DeductionMappings { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

        }
    }
}