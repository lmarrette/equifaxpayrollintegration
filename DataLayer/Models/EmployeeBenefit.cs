﻿// Created by:  Lee Marrette
// Date:  10/12/2018
// Solution:  EquifaxPayrollIntegration
// File:  EmployeeBenefit.cs

namespace DataLayer.Models
{
    /// <summary>
    /// Class to store employee benefit plans and coverage
    /// </summary>
    public class EmployeeBenefit
    {
        public int    Id          { get; set; }
        public string EmployeeId  { get; set; }
        public string BenefitPlan { get; set; }
        public string Coverage    { get; set; }
    }
}