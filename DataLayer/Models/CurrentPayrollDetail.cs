﻿// Created by:  Lee Marrette
// Date:  10/11/2018
// Solution:  EquifaxPayrollIntegration
// File:  CurrentEarning.cs

using System;

namespace DataLayer.Models
{
    /// <summary>
    ///     Store earning detail for current period run
    /// </summary>
    public class CurrentPayrollDetail
    {
        public int      Id               { get; set; }
        public string   EmployeeId       { get; set; }
        public string   PayGroupDetail   { get; set; }
        public DateTime PeriodStartDate  { get; set; }
        public DateTime PeriodEndDate    { get; set; }
        public DateTime PayDate          { get; set; }
        public string   DeductionCode    { get; set; }
        public string   EarningCode      { get; set; }
        public decimal  ResultLineAmount { get; set; }
        public decimal  ProratedHours    { get; set; }
        public decimal  UnProratedHours  { get; set; }
        public string   EmployerPaid     { get; set; }
        public bool     ValidCode        { get; set; }
    }
}