﻿// Created by:  Lee Marrette
// Date:  10/12/2018
// Solution:  EquifaxPayrollIntegration
// File:  PayrollStaging.cs

namespace DataLayer.Models
{
    /// <summary>
    /// Class to hold incoming payroll data
    /// </summary>
    public class PayrollStaging
    {
        public int Id { get; set; }
        public string EmployeeId { get; set; }
        public string PeriodStartDate { get; set; }
        public string PeriodEndDate { get; set; }
        public string PayDate { get; set; }
        public string GrossPay { get; set; }
        public string NetPay { get; set; }
        public string SocialSecurityNumber { get; set; }
        public string UserName { get; set; }
        public string PayGroup { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string HireDate { get; set; }
        public string ContinuousServiceDate { get; set; }
        public string JobTitle { get; set; }
        public string ActiveStatus { get; set; }
        public string OriginalHireDate { get; set; }
        public string TimeType { get; set; }
        public string TerminationDate { get; set; }
        public string HomeAddressLine1 { get; set; }
        public string HomeAddressLine2 { get; set; }
        public string HomeAddressCity { get; set; }
        public string HomeAddressState { get; set; }
        public string HomeAddressPostalCode { get; set; }
        public string DateOfBirth { get; set; }
        public string PeriodEndDateLastRegularPayPeriod { get; set; }
        public string WorkAddressLine1 { get; set; }
        public string WorkAddressLine2 { get; set; }
        public string WorkCity { get; set; }
        public string WorkState { get; set; }
        public string WorkPostalCode { get; set; }
        public string HourlyRate { get; set; }
        public string TotalBasePayAmount { get; set; }
        public string CompanyTaxId { get; set; }
        public string DeductionCode { get; set; }
        public string EarningCode { get; set; }
        public string ResultLineAmount { get; set; }
        public string BalanceYtd { get; set; }
        public string HoursProrated { get; set; }
        public string HoursUnprorated { get; set; }
        public string EmployerPaid { get; set; }

    }
}