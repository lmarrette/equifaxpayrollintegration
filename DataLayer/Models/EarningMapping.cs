﻿// Created by:  Lee Marrette
// Date:  10/12/2018
// Solution:  EquifaxPayrollIntegration
// File:  EarningMapping.cs

namespace DataLayer.Models
{
    /// <summary>
    /// Class to map current period and ytd earning codes to Equifax Categories
    /// </summary>
    public class EarningMapping
    {
        public int    Id             { get; set; }
        public string Earning        { get; set; }
        public string CurrentMapping { get; set; }
        public string YtdMapping     { get; set; }
    }
}