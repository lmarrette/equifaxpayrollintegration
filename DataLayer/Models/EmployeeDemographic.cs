﻿// Created by:  Lee Marrette
// Date:  10/11/2018
// Solution:  EquifaxPayrollIntegration
// File:  EmployeeDemographics.cs

namespace DataLayer.Models
{
    /// <summary>
    /// Store employee demographics for the current payroll run
    /// </summary>
    public class EmployeeDemographic
    {
        public int    Id                            { get; set; }
        public string EmployeeId                    { get; set; }
        public string SocialSecurityNumber          { get; set; }
        public string UserName                      { get; set; }
        public string PayGroup                      { get; set; }
        public string FirstName                     { get; set; }
        public string MiddleName                    { get; set; }
        public string LastName                      { get; set; }
        public string HireDate                      { get; set; }
        public string ContinuousServiceDate         { get; set; }
        public string JobTitle                      { get; set; }
        public string ActiveStatus                  { get; set; }
        public string OriginalHireDate              { get; set; }
        public string TimeType                      { get; set; }
        public string TerminationDate               { get; set; }
        public string HomeAddressLine1              { get; set; }
        public string HomeAddressLine2              { get; set; }
        public string HomeAddressCity               { get; set; }
        public string HomeAddressState              { get; set; }
        public string HomeAddressPostalCode         { get; set; }
        public string DateOfBirth                   { get; set; }
        public string PeriodEndLastRegularPayPeriod { get; set; }
        public string WorkAddressLine1              { get; set; }
        public string WorkAddressLine2              { get; set; }
        public string WorkCity                      { get; set; }
        public string WorkState                     { get; set; }
        public string WorkPostalCode                { get; set; }
        public string HourlyRate                    { get; set; }
        public string TotalBasePay                  { get; set; }
        public string CompanyTaxId                  { get; set; }
    }
}