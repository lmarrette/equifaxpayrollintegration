﻿// Created by:  Lee Marrette
// Date:  10/11/2018
// Solution:  EquifaxPayrollIntegration
// File:  YearToDateEarning.cs

using System;

namespace DataLayer.Models
{
    public class YearToDateEarning
    {
        public int      Id             { get; set; }
        public string   EmployeeId     { get; set; }
        public DateTime PayDate        { get; set; }
        public string   Earning        { get; set; }
        public string   EarningMapping { get; set; }
        public decimal  YtdAmount      { get; set; }
    }
}