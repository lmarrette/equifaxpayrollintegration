﻿// Created by:  Lee Marrette
// Date:  10/12/2018
// Solution:  EquifaxPayrollIntegration
// File:  DeductionMapping.cs

namespace DataLayer.Models
{
    /// <summary>
    /// Class to map current period and ytd deduction codes to Equifax Categories
    /// </summary>
    public class DeductionMapping
    {
        public int    Id             { get; set; }
        public string Deduction      { get; set; }
        public string CurrentMapping { get; set; }
        public string YtdMapping     { get; set; }
    }
}