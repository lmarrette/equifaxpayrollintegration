﻿// Created by:  Lee Marrette
// Date:  10/12/2018
// Solution:  EquifaxPayrollIntegration
// File:  PayrollStagingDb.cs

using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity.Validation;
using System.Linq;
using DataLayer.Models;

using log4net.Config;

namespace DataLayer
{
    /// <summary>
    /// Code database access for payroll data
    /// </summary>
    public class PayrollStagingDb
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private       ContextEquifaxPayroll _db;
        private const string         defaultMapping = "Other";

        public PayrollStagingDb()
        {
            _db = new ContextEquifaxPayroll();
        }

        /// <summary>
        /// Truncate payroll staging table before current load
        /// </summary>
        public void ClearStagingTable()
        {
            try
            {
                _db.Database.ExecuteSqlCommand("TRUNCATE TABLE PayrollStagings");
            }
            catch (DbException e)
            {
                log.Error("Database exception clearing staging table: " + e);
                throw;
            }
            catch (Exception e)
            {
                log.Error("General exception clearing staging table: " + e);
                throw;
            }
        }

        //public void InsertPayrollStagingData(PayrollStaging payData)
        //{
        //    try
        //    {
        //        _db.PayrollStagings.Add(payData);
        //        _db.SaveChanges();
        //    }
        //    catch (DbEntityValidationException dbEntity)
        //    {
        //        foreach (var errors in dbEntity.EntityValidationErrors)
        //        {
        //            foreach (var validationError in errors.ValidationErrors)
        //            {
        //                log.Error(
        //                          "Property: " + validationError.PropertyName +
        //                          " Error: " +
        //                          validationError.ErrorMessage + " For Employee:  " +
        //                          payData.EmployeeId);
        //            }
        //        }

        //        throw;
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error("General Exception at InsertPayrollStagingData: " + e);
        //        throw;
        //    }
        //}

        ///// <summary>
        ///// Return all staging rows for further processing
        ///// </summary>
        ///// <returns></returns>
        //public IOrderedEnumerable<PayrollStaging> ReturnAllStagingRecords()
        //{
        //    try
        //    {
        //        return _db.PayrollStagings.ToList().OrderBy(x => x.EmployeeId);
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error("Exception retrieving Payroll Staging data: " + e);
        //        throw;
        //    }
        //}

        ///// <summary>
        ///// Load employee demographic table with one row per employee
        ///// </summary>
        //public void LoadDemographicTable()
        //{
        //    _db.Database.ExecuteSqlCommand("procLoadEmployeeDemographicTable");
        //}


        ///// <summary>
        ///// Return payroll header information
        ///// </summary>
        ///// <returns></returns>
        //public IOrderedEnumerable<PayrollHeader> ReturnPayrollHeaders()
        //{
        //    return _db.PayrollHeaders.ToList().OrderBy(e => e.EmployeeId)
        //        .ThenBy(e => e.PeriodStartDate).ThenBy(e => e.PeriodEndDate)
        //        .ThenBy(e => e.PayDate);
        //}

        ///// <summary>
        ///// Find employee demographic data using employee id
        ///// </summary>
        ///// <param name="payrollEmployeeId"></param>
        ///// <returns></returns>
        //public EmployeeDemographic FindEmployeeDemographics(string payrollEmployeeId)
        //{
        //    return _db.EmployeeDemographics.FirstOrDefault(e => e.EmployeeId ==
        //                                                        payrollEmployeeId);
        //}

        //public List<PayrollDetail> ReturnPayrollDetail(string headerEmployeeId,
        //                                               string headerPeriodStartDate,
        //                                               string headerPeriodEndDate,
        //                                               string headerPayDate)
        //{
        //    return _db.PayrollDetails.Where(e => e.EmployeeId == headerEmployeeId &&
        //                                         e.PeriodStartDate ==
        //                                         headerPeriodStartDate &&
        //                                         e.PeriodEndDate == headerPeriodEndDate &&
        //                                         e.ValidCode == true &&
        //                                         e.PayDate == headerPayDate).ToList();
        //}

        ///// <summary>
        ///// Populate the master detail and demographic tables
        ///// </summary>
        //public void LoadMasterDetailTables()
        //{
        //    try
        //    {
        //        _db.Database.ExecuteSqlCommand("procLoadEmployeeDemographicTable");
        //        _db.Database.ExecuteSqlCommand("procLoadPayrollHeaderAndDetail");
        //    }
        //    catch (DbException e)
        //    {
        //        log.Error("Db Exception loading master detail tables: " + e);
        //        throw;
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error("General Exception loading master detail tables: " + e);
        //        throw;
        //    }
        //}

        //public string ReturnPayFrequency(string payGroup,
        //                                 string payPeriodStart,
        //                                 string payPeriodEnd,
        //                                 string state)
        //{
        //    if (string.IsNullOrEmpty(payGroup))
        //    {
        //        var payFrequency =
        //            DetermineFrequencyWithoutPayGroup(payPeriodStart, payPeriodEnd, state);
        //        return payFrequency;
        //    }
        //    var frequency =
        //        _db.PayFrequencies.FirstOrDefault(f => f.PayGroup == payGroup);
        //    if (frequency != null) return frequency.PayFrequencyCode;
        //    return string.Empty;
        //}
    }
}